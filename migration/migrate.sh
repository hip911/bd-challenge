#!/bin/bash

mysql -h $DB_HOST -u $DB_USERNAME -p$DB_PASSWORD < 'create_db.sql'
cd /var/www/html && php artisan migrate
