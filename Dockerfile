FROM php:7.1-apache

ARG PUID=33
ARG PGID=33

RUN a2enmod rewrite && docker-php-ext-install pdo_mysql

ENV APACHE_DOCUMENT_ROOT /var/www/html/public
RUN mkdir -p $APACHE_DOCUMENT_ROOT

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

COPY app/ /var/www/html/

RUN groupmod -g $PGID www-data \
    && usermod -u $PUID www-data

RUN chown -R www-data:www-data /var/www && chmod 755 /var/www
