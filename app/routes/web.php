<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
/** @var $db \Illuminate\Database\DatabaseManager */
$db = app('db');

$router->get('healthz', function () {
    return response('', 200);
});


$router->get('hello/{name}', function ($name) use ($db) {

    if (!ctype_alpha($name)) {
        return response(['error' => 'Please provide a valid name'], 400);
    }

    $results = $db->select('SELECT * FROM birthdays where name = :name;', ['name' => $name]);

    if (!$results) {
        return response(['error' => 'Not Found'], 404);
    }

    $birthday = $results[0]->birthday;
    $storedBirthday = DateTime::createFromFormat("Y-m-d", $birthday)->format('Y-m-d');
    $today = (new \DateTime())->format('Y-m-d');
    $todayPlusFiveDays = (new \DateTime())->add(new DateInterval('P5D'))->format('Y-m-d');

    if ($storedBirthday === $todayPlusFiveDays) {
        return response(['message' => 'Hello, ' . $name . '! Your birthday is in 5 days'], 200);
    }

    if ($storedBirthday === $today) {
        return response(['message' => 'Hello, ' . $name . '! Happy birthday!'], 200);
    }

});

$router->put('hello/{name}', function ($name, Request $request) use ($db) {

    if (!ctype_alpha($name)) {
        return response(['error' => 'Please provide a valid name'], 400);
    }

    $dateOfBirth = $request->input('dateOfBirth');
    $conversion = DateTime::createFromFormat("Y-m-d", $dateOfBirth);
    if (!($conversion !== false && !array_sum($conversion->getLastErrors()))) {
        return response(['error' => 'Not a valid date'], 400);
    }

    $db->insert('
        INSERT INTO birthdays (name,birthday) VALUES (:name, :birthdayForInsert) 
        ON DUPLICATE KEY UPDATE birthday=:birthdayForUpdate',
        [
            'name' => $name,
            'birthdayForInsert' => $dateOfBirth,
            'birthdayForUpdate' => $dateOfBirth
        ]
    );

    return response('', 201);
});
