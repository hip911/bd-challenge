# Birthday App

The miniature application has been optimized for TTUTC (time to understand the code), did not want to use tricky framework implementations (eg validation) so apart the migration all the custom code is in routes/web.php

For anything larger it is highly advised to create a controller to handle http level logic, services to extract business logic from the controller, use dependency injection on need.

On the GET /hello/{name} route there was no predefined message when the birthday is not today or in 5 days, so falling back to framework default of no body and a response code of 200.

Names are case insensitive and there is validation for alpha characters only also there is validation for the date, which results in responses with 'error' attribute and 400 response code.
